#define _GNU_SOURCE

#include <assert.h>
#include <fcntl.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <threads.h>
#include <unistd.h>

#define LOG_SSIZE 4096
#define MAGIC_ID 0xdeadbeef0000000

_Atomic int lr_thread_counter = 1;

thread_local struct metalog tlslog;

struct metalog {
	size_t size, pos;
	struct log_op *log;
};


struct lr_thrd_args {
	int id;
	thrd_start_t func;
	void *arg;
};

enum operation {head, cass, casw, str, ldd, xcg, thread, mal, fre};

struct head_args {
	size_t magic_id;
};

struct cas_args {
	size_t ptr;
	size_t expected;
	size_t content;
	size_t desired;
	size_t success;
};

struct str_args {
	size_t ptr;
	size_t val;
};

struct ldd_args {
	size_t ptr;
	size_t val;
};

struct xcg_args {
	size_t ptr;
	size_t in;
	size_t out;
};

struct thread_args {
	size_t id;
};

struct mal_args {
	void *ptr;
	size_t size;
};

struct fre_args {
	void *ptr;
};

struct log_op {
	enum operation type;
	union {
		struct head_args head;
		struct cas_args cas;
		struct str_args str;
		struct ldd_args ldd;
		struct xcg_args xcg;
		struct thread_args thread;
		struct mal_args mal;
		struct fre_args fre;
	};
};

void grow_log()
{
	while(tlslog.size - (tlslog.pos * sizeof(struct log_op)) < sizeof(struct log_op)){
		void *a;
		if((a = mremap(tlslog.log, tlslog.size, tlslog.size * 2, MREMAP_MAYMOVE))){
			tlslog.log = a;
			tlslog.size *= 2;
		}
	}
}


void init_main() __attribute__((constructor));

void init_main() {
	tlslog.log = mmap(NULL, LOG_SSIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	assert(tlslog.log);
	tlslog.log[0].type = head;
	tlslog.log[0].head.magic_id = MAGIC_ID;
	tlslog.pos = 1;
	tlslog.size = LOG_SSIZE;
}


int lr_init_thread(void *arg) {
	struct lr_thrd_args *larg = arg;
	tlslog.log = mmap(NULL, LOG_SSIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	assert(tlslog.log);
	tlslog.log[0].type = head;
	tlslog.log[0].head.magic_id = MAGIC_ID + larg->id;
	tlslog.pos = 1;
	tlslog.size = LOG_SSIZE;
	return larg->func(larg->arg);
}


int lr_thrd_create(thrd_t *id, thrd_start_t func, void * arg)
{
	struct lr_thrd_args *args = malloc(sizeof(struct lr_thrd_args));
	args->id = atomic_fetch_add(&lr_thread_counter, 1);
	grow_log();
	tlslog.log[tlslog.pos].type = thread;
	tlslog.log[tlslog.pos].thread.id = args->id;
	tlslog.pos++;
	args->func = func;
	args->arg = arg;
	return thrd_create(id, lr_init_thread, args);
}

void pos_cass(size_t ptr, size_t exp, size_t val, size_t des, int r, char *fname, int line)
{
	grow_log();
	tlslog.log[tlslog.pos].type = cass;
	tlslog.log[tlslog.pos].cas.ptr = ptr;
	tlslog.log[tlslog.pos].cas.expected = exp;
	tlslog.log[tlslog.pos].cas.content = val;
	tlslog.log[tlslog.pos].cas.desired = des;
	tlslog.log[tlslog.pos].cas.success = r;
	tlslog.pos++;
}

void pos_casw(size_t ptr, size_t exp, size_t val, size_t des, int r, char *fname, int line)
{
	grow_log();
	tlslog.log[tlslog.pos].type = casw;
	tlslog.log[tlslog.pos].cas.ptr = ptr;
	tlslog.log[tlslog.pos].cas.expected = exp;
	tlslog.log[tlslog.pos].cas.content = val;
	tlslog.log[tlslog.pos].cas.desired = des;
	tlslog.log[tlslog.pos].cas.success = r;
	tlslog.pos++;
}

void pos_str(size_t ptr, size_t val, char *fname, int line)
{
	grow_log();
	tlslog.log[tlslog.pos].type = str;
	tlslog.log[tlslog.pos].str.ptr = ptr;
	tlslog.log[tlslog.pos].str.val = val;
	tlslog.pos++;
}

void pos_ldd(size_t ptr, size_t val, char *fname, int line)
{
	grow_log();
	tlslog.log[tlslog.pos].type = ldd;
	tlslog.log[tlslog.pos].ldd.ptr = ptr;
	tlslog.log[tlslog.pos].ldd.val = val;
	tlslog.pos++;
}

void pos_xcg(size_t ptr, size_t in, size_t out, char *fname, int line)
{
	grow_log();
	tlslog.log[tlslog.pos].type = xcg;
	tlslog.log[tlslog.pos].xcg.ptr = ptr;
	tlslog.log[tlslog.pos].xcg.in = in;
	tlslog.log[tlslog.pos].xcg.out = out;
	tlslog.pos++;
}

void *lr_malloc(size_t size) {
	void *alloc = malloc(size);
	grow_log();
	tlslog.log[tlslog.pos].type = mal;
	tlslog.log[tlslog.pos].mal.ptr = alloc;
	tlslog.log[tlslog.pos].mal.size = size;
	return alloc;
}

void lr_free(void *ptr) {
	free(ptr);
	grow_log();
	tlslog.log[tlslog.pos].type = fre;
	tlslog.log[tlslog.pos].mal.ptr = ptr;
}
