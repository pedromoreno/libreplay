#ifndef __LIBREPLAY_H__
#define __LIBREPLAY_H__

#include <threads.h>
#include <stdatomic.h>


int lr_thrd_create(thrd_t *id, thrd_start_t func, void * arg);

void pos_cass(size_t ptr, size_t exp, size_t val, size_t des, int r, char *fname, int line);

void pos_casw(size_t ptr, size_t exp, size_t val, size_t des, int r, char *fname, int line);

void pos_str(size_t ptr, size_t val, char *fname, int line);

void pos_ldd(size_t ptr, size_t val, char *fname, int line);

void pos_xcg(size_t ptr, size_t in, size_t out, char *fname, int line);

void *lr_malloc(size_t size);

void lr_free(void *ptr);

#undef thrd_create
#define thrd_create lr_thrd_create

#undef malloc
#define malloc lr_malloc

#undef free
#define free lr_free




#undef atomic_compare_exchange_strong
#undef atomic_compare_exchange_strong_explicit

#define atomic_compare_exchange_strong_explicit(PTR, VAL, DES, SUC, FAIL) \
 __extension__ \
({ \
	__auto_type __atomic_compare_exchange_ptr = (PTR); \
	__typeof__ (*__atomic_compare_exchange_ptr) __atomic_compare_exchange_tmp = (DES); \
	size_t exp = (size_t) *VAL; \
	int r = __atomic_compare_exchange (__atomic_compare_exchange_ptr, (VAL), &__atomic_compare_exchange_tmp, 0, (SUC), (FAIL)); \
	pos_cass((size_t)PTR, exp, (size_t)*VAL, (size_t)DES, r, __FILE__, __LINE__); \
	r; \
})

#define atomic_compare_exchange_strong(addr, exp, des) atomic_compare_exchange_strong_explicit(addr, exp, des, memory_order_seq_cst, memory_order_seq_cst)


#undef atomic_compare_exchange_weak
#undef atomic_compare_exchange_weak_explicit

#define atomic_compare_exchange_weak_explicit(PTR, VAL, DES, SUC, FAIL) \
 __extension__ \
({ \
	__auto_type __atomic_compare_exchange_ptr = (PTR); \
	__typeof__ (*__atomic_compare_exchange_ptr) __atomic_compare_exchange_tmp = (DES); \
	size_t exp = (size_t) *VAL; \
	int r = __atomic_compare_exchange (__atomic_compare_exchange_ptr, (VAL), &__atomic_compare_exchange_tmp, 1, (SUC), (FAIL)); \
	pos_casw((size_t)PTR, exp, (size_t)*VAL, (size_t)DES, r, __FILE__, __LINE__); \
	r; \
})

#define atomic_compare_exchange_weak(addr, exp, des) atomic_compare_exchange_weak_explicit(addr, exp, des, memory_order_seq_cst, memory_order_seq_cst)


#undef atomic_store
#undef atomic_store_explicit

#define atomic_store_explicit(PTR, VAL, MO) \
__extension__ \
({ \
	__auto_type __atomic_store_ptr = (PTR); \
	__typeof__ (*__atomic_store_ptr) __atomic_store_tmp = (VAL); \
	__atomic_store (__atomic_store_ptr, &__atomic_store_tmp, (MO)); \
	pos_str((size_t)PTR, (size_t)VAL, __FILE__, __LINE__); \
  })

#define atomic_store(PTR, VAL) atomic_store_explicit (PTR, VAL, __ATOMIC_SEQ_CST)


#undef atomic_load
#undef atomic_load_explicit

#define atomic_load_explicit(PTR, MO) \
__extension__ \
({ \
	__auto_type __atomic_load_ptr = (PTR); \
	__typeof__ (*__atomic_load_ptr) __atomic_load_tmp; \
	__atomic_load (__atomic_load_ptr, &__atomic_load_tmp, (MO)); \
	pos_ldd((size_t)PTR, (size_t)__atomic_load_tmp, __FILE__, __LINE__); \
	__atomic_load_tmp; \
  })

#define atomic_load(PTR) atomic_load_explicit (PTR, __ATOMIC_SEQ_CST)


#undef atomic_exchange
#undef atomic_exchange_explicit

#define atomic_exchange_explicit(PTR, VAL, MO) \
__extension__ \
({ \
	__auto_type __atomic_exchange_ptr = (PTR); \
	__typeof__ (*__atomic_exchange_ptr) __atomic_exchange_val = (VAL); \
	__typeof__ (*__atomic_exchange_ptr) __atomic_exchange_tmp; \
	__atomic_exchange (__atomic_exchange_ptr, &__atomic_exchange_val, &__atomic_exchange_tmp, (MO)); \
	pos_xcg((size_t)PTR, (size_t)VAL, (size_t)__atomic_exchange_tmp, __FILE__, __LINE__); \
	__atomic_exchange_tmp; \
  })

#define atomic_exchange(PTR, VAL) atomic_exchange_explicit (PTR, VAL, __ATOMIC_SEQ_CST)


#undef atomic_fetch_add
#undef atomic_fetch_add_explicit

#define atomic_fetch_add_explicit(PTR, VAL, MO)  \
			  __atomic_fetch_add ((PTR), (VAL), (MO))

#define atomic_fetch_add(PTR, VAL) atomic_fetch_add_explicit ((PTR), (VAL), __ATOMIC_SEQ_CST)


#undef atomic_fetch_sub
#undef atomic_fetch_sub_explicit

#define atomic_fetch_sub_explicit(PTR, VAL, MO)  \
			  __atomic_fetch_sub ((PTR), (VAL), (MO))

#define atomic_fetch_sub(PTR, VAL) atomic_fetch_sub_explicit ((PTR), (VAL), __ATOMIC_SEQ_CST)


#undef atomic_fetch_or
#undef atomic_fetch_or_explicit

#define atomic_fetch_or_explicit(PTR, VAL, MO)  \
			  __atomic_fetch_or ((PTR), (VAL), (MO))

#define atomic_fetch_or(PTR, VAL) atomic_fetch_or_explicit ((PTR), (VAL), __ATOMIC_SEQ_CST)


#undef atomic_fetch_xor
#undef atomic_fetch_xor_explicit

#define atomic_fetch_xor_explicit(PTR, VAL, MO)  \
			  __atomic_fetch_xor ((PTR), (VAL), (MO))

#define atomic_fetch_xor(PTR, VAL) atomic_fetch_xor_explicit ((PTR), (VAL), __ATOMIC_SEQ_CST)


#undef atomic_fetch_and
#undef atomic_fetch_and_explicit

#define atomic_fetch_and_explicit(PTR, VAL, MO)  \
			  __atomic_fetch_and ((PTR), (VAL), (MO))

#define atomic_fetch_and(PTR, VAL) atomic_fetch_and_explicit ((PTR), (VAL), __ATOMIC_SEQ_CST)


#endif // __LIBREPLAY_H__
